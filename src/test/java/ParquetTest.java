import org.apache.hadoop.fs.Path;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;
import parquet.hadoop.api.WriteSupport;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;

import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doReturn;
import static org.powermock.api.mockito.PowerMockito.spy;

public class ParquetTest
{
    @Test
    public void testSimple() throws IOException
    {
        CsvToParquet csvToParquet = new CsvToParquet();
        CsvToParquet csvSpy = spy(csvToParquet);
        String input = "4000625400000001846,ACT-XPO,Closed,No Competency,1-Apr-04,30-Sep-04,0.49,EV - Other,Activ Training,United Kingdom,1,1";
        Path test = new Path("test.parquet") ;

        BufferedReader bufferedReader = Mockito.mock(BufferedReader.class);
        Mockito.when(bufferedReader.readLine()).thenReturn(input).thenReturn(null);
        doReturn(bufferedReader).when(csvSpy).getInputFile();

        doReturn(test).when(csvSpy).getOutput();

        CsvParquetWriter csvParquetWriter = Mockito.mock(CsvParquetWriter.class);
        doReturn(csvParquetWriter).when(csvSpy).getCsvParquetWriter(Matchers.<WriteSupport<List<String>>>any());

        csvSpy.run();
        verify(csvParquetWriter, times(1)).write(anyListOf(String.class));
    }

    @After
    public void after() throws IOException
    {
        File file =new File("test.parquet");
       Files.deleteIfExists(file.toPath());
    }

    @Before
    public void before() throws IOException
    {
        File file =new File("test.parquet");
        Files.deleteIfExists(file.toPath());
    }

}
