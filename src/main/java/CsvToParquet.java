import org.apache.hadoop.fs.Path;
import parquet.hadoop.api.WriteSupport;
import parquet.schema.MessageType;
import parquet.schema.MessageTypeParser;

import java.io.*;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public class CsvToParquet {
    private static final Logger LOGGER = Logger.getLogger(CsvToParquet.class.getName());
    private static final String SEPARATOR = ",";
    private static String firstArgv;
    private static String secondArgv;

    public static void main(String[] args) {
        CsvToParquet csvToParquet = new CsvToParquet();
        if (args.length != 2) {
            System.err.println("in/out missed");
            System.exit(2);
        }
        firstArgv = args[0];
        secondArgv = args[1];
        csvToParquet.run();
    }

    protected void run()
    {
        try {
            String firstLine = "message csv {required binary PROJECT_ID = 1;" +
                    "required binary PROJECT = 2;" +
                    "required binary PROJECT_STATE = 3;" +
                    "required binary PRIMARY_COMPETENCY_NAME = 4;" +
                    "required binary PROJECT_START_DATE = 5;" +
                    "required binary PROJECT_END_DATE = 6;" +
                    "required binary PROJECT_DURATION_IN_YEARS = 7;" +
                    "required binary PROJECT_INDUSTRY = 8;" +
                    "required binary CUSTOMER = 9;" +
                    "required binary CUSTOMER_COUNTRY = 10;" +
                    "required binary MAX_EMPLOYEES_ON_PROJECT = 11;" +
                    "required binary MEDIAN = 12;}\n";

            MessageType messageType = MessageTypeParser.parseMessageType(firstLine);
            WriteSupport<List<String>> writeSupport = new CsvWriteSupport(messageType);
            String line;
            try (CsvParquetWriter writer = getCsvParquetWriter(writeSupport);
                 BufferedReader br = getInputFile() )
            {
                while ((line = br.readLine()) != null)
                {
                    String[] fields = line.split(Pattern.quote(SEPARATOR));
                    writer.write(Arrays.asList(fields));
                }
            }
        }
        catch (IOException e)
        {
            LOGGER.log(Level.SEVERE, "Failed to write parquet file: " + e.getMessage(), e);
        }
    }

    //added for unit test mock
    protected BufferedReader getInputFile() throws FileNotFoundException
    {
        java.nio.file.Path csvPath = Paths.get(firstArgv);
        return  new BufferedReader(new FileReader(csvPath.toFile()));
    }

    //added for unit test mock
    protected Path getOutput()
    {
        return new Path(secondArgv);
    }

    //added for unit test mock
    protected  CsvParquetWriter getCsvParquetWriter( WriteSupport<List<String>> writeSupport) throws IOException
    {
        return new CsvParquetWriter(getOutput(), writeSupport);
    }
}